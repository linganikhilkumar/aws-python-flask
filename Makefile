SHELL:=/bin/bash

.DEFAULT_GOAL := help

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) |  awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

CWD = $(shell pwd)
GIT_COMMIT ?= $(shell git rev-parse HEAD)
BRANCH_NAME ?= $(shell git symbolic-ref --short HEAD)
SERVICE_NAME := digital-hemming
AWS_REGION := us-west-2
####################################################################
# Jenkins required commands
.PHONY: install
install: 	## install the dependencies
	PIPENV_IGNORE_VIRTUALENVS=1 pipenv run pipenv sync --dev
	PIPENV_IGNORE_VIRTUALENVS=1 pipenv run pipenv graph

.PHONY: lock
lock: 	## renews lock file
	PIPENV_IGNORE_VIRTUALENVS=1 pipenv run pipenv lock

.PHONY: lint
lint:		## lint the project
	env
	PIPENV_IGNORE_VIRTUALENVS=1 pipenv run pylint todos/

####################################################################

.PHONY: up
up: ## Invoke Migrations lambda
	@echo "run migrations"
	aws lambda invoke \
        --function-name $(SERVICE_NAME)-$(STAGE)-migrations-up \
        --payload '{}' --region $(AWS_REGION) \
			response.json

.PHONY: transition
transition: ## Invoke Migrate transition lambda
	@echo "make transitions"
	aws lambda invoke \
        --function-name $(SERVICE_NAME)-$(STAGE)-migrate-transition-data \
        --payload '{}' --region $(AWS_REGION) \
			response.json

.PHONY: package
package:  ## Packages the stack locally
	serverless package --stage $(STAGE)

.PHONY: deploy
deploy:	## deploys the stack on AWS
	serverless deploy --stage $(STAGE) --region $(AWS_REGION)

.PHONY: postgres
postgres:	## creates a docker postgres container
	docker-compose up -d

.PHONY: migrate-local
migrate-local:	## does local migrations
	python digitalHems/manage.py migrate

.PHONY: transition-local
transition-local:	## put data into transition table
	python digitalHems/transitionHandler.py

.PHONY: integration-test
integration-test: transition-local		## run test
	(cd digitalHems/orders && PIPENV_IGNORE_VIRTUALENVS=1 pipenv run pytest -v tests.py)

.PHONY: flush
flush:		## flush the database
	python digitalHems/manage.py flush
